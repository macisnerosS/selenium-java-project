package utils.interfaces;

public interface IAccount {
    String getUsername();
    String getPassword();
    String getConfPassword();
}
