package utils.interfaces;

public interface IContact {
    String getName();
    String getLastname();
    String getPhone();
    String getEmail();
}
