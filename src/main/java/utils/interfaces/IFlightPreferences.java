package utils.interfaces;

public interface IFlightPreferences {
    Integer getService();
    String getAirline();
}
