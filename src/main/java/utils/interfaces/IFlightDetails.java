package utils.interfaces;

public interface IFlightDetails {
    Integer getType();
    String getPassengers();
    String getDepartingFrom();
    String getOn();
    String getArrivingIn();
    String getReturning();
}
