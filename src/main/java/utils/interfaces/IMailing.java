package utils.interfaces;

public interface IMailing {
    String getAddress();
    String getCity();
    String getState();
    String getPostalCode();
    String getCountry();
}
