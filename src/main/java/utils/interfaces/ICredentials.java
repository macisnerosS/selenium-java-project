package utils.interfaces;

public interface ICredentials {
    String getUsername();
    String getPassword();
}
