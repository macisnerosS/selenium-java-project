package utils;

import utils.enums.CommandLine;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class Encrypter {

    public static void main(String[] args) {

        try {
            String dataToEncrypt = "";

            byte[] secretKeyBytes = CommandLine.CIPHER_SECRET.value().getBytes();
            byte[] aes256Key = new byte[32];
            System.arraycopy(secretKeyBytes, 0, aes256Key, 0, 32);

            SecretKeySpec secretKeySpec = new SecretKeySpec(aes256Key, "AES");
            String encryptedData = encrypt(dataToEncrypt, secretKeySpec);

            System.out.println("Encrypted data: " + encryptedData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String decrypt(String encryptedValue) {
        try {
            byte[] secretKeyBytes = CommandLine.CIPHER_SECRET.value().getBytes();
            byte[] aes256Key = new byte[32];
            System.arraycopy(secretKeyBytes, 0, aes256Key, 0, 32);

            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(aes256Key, "AES"));

            byte[] encryptedBytes = Base64.getDecoder().decode(encryptedValue);

            byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
            return new String(decryptedBytes);
        } catch (Exception e) {
            throw new RuntimeException("Error decrypting the value: " + encryptedValue, e);
        }
    }

    private static String encrypt(String data, SecretKeySpec secretKeySpec) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
        byte[] encryptedBytes = cipher.doFinal(data.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }
}
