package utils;

import utils.enums.Browser;
import utils.enums.CommandLine;
import utils.enums.Profile;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public final class Driver {

    public static WebDriver instance() {
        Browser browserType = Enum.valueOf(Browser.class, CommandLine.BROWSER.value().toUpperCase());

        if (ThreadManager.instance().getDriver() == null) {
            WebDriver driver = createWebDriverInstance(browserType);

            ThreadManager.instance().setDriver(driver);
        }

        return ThreadManager.instance().getDriver();
    }

    public static void quitDriver() {
        if (ThreadManager.instance().getDriver() != null) {
            ThreadManager.instance().getDriver().quit();
        }
    }

    private static WebDriver createWebDriverInstance(Browser browser) {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        switch (browser) {
            case CHROME:
                WebDriverManager.chromedriver().setup();

                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--start-maximized");

                capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();

                FirefoxOptions firefoxOptions = new FirefoxOptions();
                capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, firefoxOptions);
                break;
            case EDGE:
                WebDriverManager.edgedriver().setup();

                EdgeOptions edgeOptions = new EdgeOptions();
                capabilities.setCapability(EdgeOptions.CAPABILITY, edgeOptions);
                break;
            case APP:
                getAndroidDriver(Profile.MOBILE_APP.value());
                break;
            case RESPONSIVE:
                WebDriverManager.chromedriver().setup();

                ChromeOptions responseOptions = new ChromeOptions();
                capabilities.setCapability(ChromeOptions.CAPABILITY, responseOptions);
                break;
            default:
                try {
                    throw new Exception("Browser not configured");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

        try {
            return new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static WebDriver getRemoteDriver() {
        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "6JH4C20212001760");
        dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UIAutomator2");
        dc.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");

        try {
            return new RemoteWebDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static AndroidDriver<AndroidElement> getAndroidDriver(String appDirectory) {
        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "6JH4C20212001760");
        dc.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        dc.setCapability(MobileCapabilityType.APP, appDirectory);
        //dc.setCapability(MobileCapabilityType.APP, "./apps/WhatsApp.apk");

        try {
            return new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
