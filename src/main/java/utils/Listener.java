package utils;

import org.openqa.selenium.WebDriver;
import utils.annotations.Retry;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import utils.enums.CommandLine;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;
import org.testng.internal.annotations.IAnnotationTransformer;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Listener implements ISuiteListener, ITestListener, IRetryAnalyzer, IAnnotationTransformer {
    private int retryCount = 0;
    private static final String PROVIDERS_PACKAGE = "data.providers.";
    private static final String TEMP_SUITE_PREFIX = "temp-testng-customsuite.xml";

    @Override
    public synchronized void onStart(ISuite suite) {
        ensureSuiteAndEnvMatch(suite);
        Logger.generateReport();
        System.out.println("Execution started");
    }

    @Override
    public synchronized void onTestStart(ITestResult result) {
        System.out.println("Test: " + result.getName());
        Logger.createTest(result.getName());
    }

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        if (result.getThrowable().getMessage() != null) {
            Logger.fail(
                    MarkupHelper.createLabel(
                            result.getThrowable().getMessage(), ExtentColor.RED));
        }

        if (ThreadManager.instance().getDriver() != null) {
            Logger.attachScreenshot();
        }

        Driver.quitDriver();
        Logger.flush();

        System.out.println("Test result: FAIL");
        ThreadManager.instance().removeThread();
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        Logger.pass(MarkupHelper.createLabel("Test successful", ExtentColor.GREEN));

        if (ThreadManager.instance().getDriver() != null) {
            Logger.attachScreenshot();
        }

        Driver.quitDriver();
        Logger.flush();

        System.out.println("Test result: PASS");
        ThreadManager.instance().removeThread();
    }

    @Override
    public synchronized void onFinish(ITestContext context) {
        System.out.println("Execution finished");
    }

    @Override
    public synchronized boolean retry(ITestResult result) {
        Retry annotation = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(Retry.class);

        if (annotation != null && retryCount < annotation.value()) {
            retryCount++;
            return true;
        }
        return false;
    }

    @Override
    public synchronized void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
        adjustAnnotationAccordingToEnv(annotation);
    }

    private synchronized void adjustAnnotationAccordingToEnv(ITestAnnotation annotation) {
        File folder = new File(
                System.getProperty("user.dir") + "/src/test/java/data/providers/" + CommandLine.ENV.value());

        List<String> availableProviders =
                Arrays.stream(folder.listFiles())
                        .map(file -> file.getName().replace(".java", ""))
                        .collect(Collectors.toList());

        if (!annotation.getDataProvider().isEmpty()) {
            String packageName = annotation.getDataProviderClass().getName();
            String[] nameSplitted = packageName.split("\\.");
            String dataProviderClassName = nameSplitted[nameSplitted.length - 1];

            if (availableProviders.contains(dataProviderClassName)) {
                try {
                    Class<?> newDataProvider =
                            Class.forName(
                                    PROVIDERS_PACKAGE + CommandLine.ENV.value() + "." + dataProviderClassName);
                    annotation.setDataProviderClass(newDataProvider);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(
                            "Data provider " + dataProviderClassName + " for environment " +
                                    CommandLine.ENV.value() + " not found");
                }
            } else {
                annotation.setEnabled(false);
            }
        }
        annotation.setRetryAnalyzer(Listener.class);
    }

    private synchronized void ensureSuiteAndEnvMatch(ISuite suite) {
        String currentSuiteName = suite.getXmlSuite().getFileName();

        if (!currentSuiteName.contains(TEMP_SUITE_PREFIX)) {
            if (!currentSuiteName.contains(CommandLine.ENV.value() + "-")) {
                String fileName = new File(currentSuiteName).getName();
                throw new RuntimeException(
                        "Suite " + fileName + " cannot be run in environment " +
                                CommandLine.ENV.value().toUpperCase());
            }
        }
    }
}
