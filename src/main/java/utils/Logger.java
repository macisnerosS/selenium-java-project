package utils;

import com.aventstack.extentreports.*;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// Filters help to modify the request before and after the execution
public class Logger implements Filter {

    private static ExtentReports report;

    public static void generateReport() {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_SSSS_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        String timestamp = dtf.format(now);

        ExtentSparkReporter reporter = new ExtentSparkReporter("./report.html");
        reporter.config().setReportName(timestamp);
        report = new ExtentReports();
        report.attachReporter(reporter);
        RestAssured.filters(new Logger());
    }

    public static void flush() {
        report.flush();
    }

    public static void createTest(String name) {
        ExtentTest logger = report.createTest(name);
        ThreadManager.instance().setLogger(logger);
    }

    public static void fail(Markup markupHelper) {
        ThreadManager.instance().getLogger().fail(markupHelper);
    }

    public static void pass(Markup markupHelper) {
        ThreadManager.instance().getLogger().pass(markupHelper);
    }

    public static String info(String details) {
        if (!details.contains("data:image/jpeg;base64")) {
            String cleanOutput =
                    details.replace("<br>", "\n")
                            .replace("<pre>", "")
                            .replace("</pre>", "");

            System.out.println(cleanOutput);
        }

        ThreadManager.instance().getLogger().log(Status.INFO, details);

        return details;
    }

    public static void attachScreenshot() {
        TakesScreenshot screenshot = (TakesScreenshot) ThreadManager.instance().getDriver();
        String base64Screenshot = screenshot.getScreenshotAs(OutputType.BASE64);

        try {
            Logger.info(
                    "<img src='data:image/jpeg;base64," + base64Screenshot + "'" +
                            " width='800' height='600' alt='base64'>");
        } catch (Exception e) {
            System.out.println("It was not possible to take screenshot");
        }
    }

    @Override
    public Response filter(FilterableRequestSpecification requestSpec, FilterableResponseSpecification responseSpec, FilterContext ctx) {

        String requestBody;

        if (requestSpec.getBody() != null) {
            JsonElement jsonElement = JsonParser.parseString(requestSpec.getBody());
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            requestBody = "Body: <pre>" + gson.toJson(jsonElement) + "</pre>";
        } else {
            requestBody = "Body: ";
        }

        Logger.info(
                ("Request method: " + requestSpec.getMethod() + "\n" +
                        "Request URI: " + requestSpec.getBaseUri() + "\n" +
                        "Proxy: " + requestSpec.getProxySpecification() + "\n" +
                        "Request params: " + requestSpec.getRequestParams().toString() + "\n" +
                        "Query params: " + requestSpec.getQueryParams().toString() + "\n" +
                        "Form params: " + requestSpec.getFormParams().toString() + "\n" +
                        "Headers: " + requestSpec.getHeaders().toString() + "\n" +
                        "Cookies: " + requestSpec.getCookies().toString() + "\n" +
                        "Multiparts: " + requestSpec.getMultiPartParams().toString() + "\n" +
                        requestBody + "\n")
                        .replace("\n", "<br>")
        );

        Response response = ctx.next(requestSpec, responseSpec);

        Logger.info(
                (response.getStatusLine() + "\n" +
                        response.headers().toString() + "\n" +
                        "<pre>" + response.body().prettyPrint() + "</pre>")
                        .replace("\n", "<br>")
        );

        return response;
    }
}