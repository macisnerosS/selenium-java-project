package utils.enums;

public enum CommandLine {
    ENV,
    BROWSER,
    CIPHER_SECRET;

    public String value() {
        String propertyName =
                this.name().toLowerCase()
                        .replace("_", ".");

        return System.getProperty(propertyName);
    }
}
