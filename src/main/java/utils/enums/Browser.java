package utils.enums;

public enum Browser {
    CHROME,
    FIREFOX,
    EDGE,
    APP,
    RESPONSIVE;
}
