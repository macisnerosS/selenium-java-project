package utils.enums;

import utils.Encrypter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public enum Profile {
    BASE_URL_API,
    MOBILE_APP,
    BASE_URL_WEB;

    static private Properties properties = null;

    public String value() {
        String propValue = getProperty(this.name());

        if (propValue.startsWith("ENC:")) {
            return Encrypter.decrypt(propValue.substring(4));
        } else {
            return propValue;
        }
    }

    private static Properties readProfile() {

        String propertyFilePath =
                "src/test/resources/profiles/" + CommandLine.ENV.value() + "-profile.properties";

        properties = new Properties();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(propertyFilePath));
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException(
                    CommandLine.ENV.value() + "-profile.properties not found");
        }

        return properties;
    }

    private String getProperty(String propName) {
        if (properties == null) {
            readProfile();
        }

        return Objects.requireNonNull(properties.getProperty(propName),
                "Property " + this.name() + " not found in file " +
                        CommandLine.ENV.value() + "-profile.properties");
    }
}
