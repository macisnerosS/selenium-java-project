package utils.enums;

import io.restassured.response.Response;
import utils.Logger;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public enum Schema {
    BOOKS_LIST,
    BOOK,
    CREATE_USER;

    public void matches(Response response) {
        String jsonFileName =
                this.name().toLowerCase()
                        .replace("_", "-")
                        .concat("-schema.json");

        response.then().body(matchesJsonSchemaInClasspath("schemas/" + jsonFileName))
                .onFailMessage(Logger.info("ASSERT: response matches json schema " + jsonFileName));
    }
}
