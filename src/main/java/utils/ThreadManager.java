package utils;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;

public final class ThreadManager {

    private static ThreadManager instance;
    private static final ThreadLocal<WebDriver> driver = ThreadLocal.withInitial(() -> null);
    private static final ThreadLocal<ExtentTest> logger = ThreadLocal.withInitial(() -> null);

    private ThreadManager() {
    }

    public static synchronized ThreadManager instance() {
        if (instance == null) {
            instance = new ThreadManager();
        }
        return instance;
    }

    public void removeThread() {
        driver.remove();
        logger.remove();
    }

    public void setDriver(WebDriver driver) {
        ThreadManager.driver.set(driver);
    }

    public WebDriver getDriver() {
        return driver.get();
    }

    public void setLogger(ExtentTest logger) {
        ThreadManager.logger.set(logger);
    }

    public ExtentTest getLogger() {
        return logger.get();
    }
}