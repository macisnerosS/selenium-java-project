package utils;

import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Sample {
    private static final Integer DEFAULT_SIZE = 10;
    private static final String EMAIL_FORMAT = "xxxxxxxxxxxxxxxxxxxxxxxx@xxxxxxxxx.com";
    private static final String NUMERIC_SAMPLE = "0123456789";
    private static final String ALPHANUMERIC_SAMPLE = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789";
    private static final String SPECIAL_CHARACTER_SAMPLE = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm$&=¡0123456789<>*-+~'";
    private static final Random RANDOM = new Random();


    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static String number(int size) {
        return generateBasedOn(NUMERIC_SAMPLE, size);
    }

    public static String number() {
        return generateBasedOn(NUMERIC_SAMPLE, DEFAULT_SIZE);
    }

    public static String string(int size) {
        return generateBasedOn(ALPHANUMERIC_SAMPLE, size);
    }

    public static String string() {
        return generateBasedOn(ALPHANUMERIC_SAMPLE, DEFAULT_SIZE);
    }

    public static String unicodeString(int size) {
        return generateBasedOn(SPECIAL_CHARACTER_SAMPLE, size);
    }

    public static String unicodeString() {
        return generateBasedOn(SPECIAL_CHARACTER_SAMPLE, DEFAULT_SIZE);
    }

    public static String email() {
        return Pattern.compile("[x]").matcher(EMAIL_FORMAT)
                .replaceAll(match -> String.valueOf((char) (RANDOM.nextInt(26) + 'a')));
    }

    public static String blank() {
        return "";
    }

    private static String generateBasedOn(String sample, int size) {
        return IntStream.range(0, size)
                .mapToObj(i -> sample.charAt(RANDOM.nextInt(sample.length())))
                .map(String::valueOf)
                .collect(Collectors.joining());
    }
}
