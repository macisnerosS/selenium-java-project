package utils;

import lombok.Getter;
import org.openqa.selenium.By;

@Getter
public final class Target {

    private By locator;
    private String name;

    Target(String name) {
        this.name = name;
    }

    public Target located(By locator) {
        this.locator = locator;
        return this;
    }

    public static Target name(String name) {
        return new Target(name);
    }
}
