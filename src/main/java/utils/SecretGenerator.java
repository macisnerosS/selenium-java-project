package utils;

import java.security.SecureRandom;

public class SecretGenerator {
    public static void main(String[] args) {
        try {
            String secretKey = generateSecretKey();
            System.out.println("Generated Secret Key: " + secretKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String generateSecretKey() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] key = new byte[32]; // 32 bytes (256 bits) para la clave secreta
        secureRandom.nextBytes(key);
        return bytesToHex(key);
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02X", b));
        }
        return result.toString();
    }
}
