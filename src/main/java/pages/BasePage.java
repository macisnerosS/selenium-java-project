package pages;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import utils.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import utils.Target;

import java.util.List;

public abstract class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public String isLocatedAt() {
        Logger.info(componentName() + ": located at " + driver.getCurrentUrl());

        return driver.getCurrentUrl();
    }

    protected void clickOn(Target element) {
        Logger.info(componentName() + ": click on " + element.getName());
        find(element).click();
    }

    protected void set(Target element, String value) {
        Logger.info(componentName() + ": set '" + value + "' in " + element.getName());
        find(element).sendKeys(value);
    }

    protected void get(String url) {
        Logger.info("Visit " + componentName().toLowerCase() + " page: " + url);
        driver.get(url);
    }

    protected String textOf(Target element) {
        Logger.info(componentName() + ": get text from " + element.getName());

        return find(element).getText().trim();
    }

    protected boolean isPresent(Target element) {
        Logger.info(componentName() + ": inspect whether " + element.getName() + " exists");

        return !findAll(element).isEmpty();
    }

    protected String valueOf(Target element) {
        Logger.info(componentName() + ": get value from " + element.getName());

        return find(element).getAttribute("value");
    }

    protected void selectByText(Target element, String value) {
        Logger.info(componentName() + ": select " + value + " from " + element.getName());

        new Select(find(element)).selectByVisibleText(value);
    }

    protected void selectByValue(Target element, String value) {
        Logger.info(componentName() + ": select " + value + " from " + element.getName());

        new Select(find(element)).selectByValue(value);
    }

    protected WebElement find(Target element) {
        try {
            return driver.findElement(element.getLocator());
        } catch (NoSuchElementException e) {
            Logger.fail(MarkupHelper.
                    createLabel("ERROR: Element " + element.getName() + " not found", ExtentColor.RED));
            return null;
        }
    }

    protected List<WebElement> findAll(Target element) {
        try {
            return driver.findElements(element.getLocator());
        } catch (NoSuchElementException e) {
            Logger.fail(MarkupHelper.
                    createLabel("ERROR: Elements " + element.getName() + " not found", ExtentColor.RED));
            return null;
        }
    }

    protected void selectRadioOptionByIndex(Target element, int index) {
        Logger.info(componentName() + ": select option " + index + " from " + element.getName());
        findAll(element).get(index).click();
    }

    private String componentName() {
        String[] words = this.getClass().getName().split("\\.");
        String context = words[words.length - 1].replace("Page", "");

        return context.toUpperCase();
    }
}
