package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Target;

public final class NoSeatsPage extends BasePage {

    private final Target LBL_TITLE =
            Target.name("no seat available label")
                    .located(By.xpath("//font[@size='4']"));
    private final Target LBL_DESCRIPTION =
            Target.name("description label")
                    .located(By.xpath("//font[@size='2'][contains(text(), 'press')]"));

    NoSeatsPage(WebDriver driver) {
        super(driver);
    }

    public boolean isDisplayed() {
        return isPresent(LBL_TITLE) && isPresent(LBL_DESCRIPTION);
    }
}
