package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Target;

public final class SidebarPage extends BasePage {

    private final Target TAB_HOME =
            Target.name("home tab")
                    .located(By.xpath("//a[.='Home']"));
    private final Target TAB_FLIGHTS =
            Target.name("flights tab")
                    .located(By.xpath("//a[.='Flights']"));

    public SidebarPage(WebDriver driver) {
        super(driver);
    }

    public void selectHomeTab() {
        clickOn(TAB_HOME);
    }

    public void selectFlightsTab() {
        clickOn(TAB_FLIGHTS);
    }
}
