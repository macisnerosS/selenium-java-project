package pages;

import utils.Logger;
import utils.enums.Profile;
import utils.interfaces.ICredentials;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Target;

public final class HomePage extends BasePage {

    private final Target TXT_USERNAME =
            Target.name("user name textbox")
                    .located(By.xpath("//input[@name='userName']"));
    private final Target TXT_PASSWORD =
            Target.name("password textbox")
                    .located(By.xpath("//input[@name='password']"));
    private final Target BTN_SUBMIT =
            Target.name("submit button")
                    .located(By.xpath("//input[@name='submit']"));
    private final Target LBL_ENTER_CREDS_CORRECT =
            Target.name("Enter your userName and password correct label")
                    .located(By.xpath("//input[@name='password']/following::span"));
    private final Target LBL_LOGIN_SUCCESS =
            Target.name("login successfully label")
                    .located(By.xpath("//h3[text()='Login Successfully']"));

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage visit() {
        get(Profile.BASE_URL_WEB.value() + "/test/newtours/index.php");

        return this;
    }

    public HomePage fillUpCredentials(ICredentials credentials) {
        set(TXT_USERNAME, credentials.getUsername());
        set(TXT_PASSWORD, credentials.getPassword());

        return this;
    }

    public HomePage login(ICredentials credentials) {
        Logger.info("----------Login----------");
        fillUpCredentials(credentials);
        clickOn(BTN_SUBMIT);

        return this;
    }

    public HomePage loginWithWrongCreds(ICredentials credentials) {
        Logger.info("----------Login----------");
        fillUpCredentials(credentials);
        clickOn(BTN_SUBMIT);

        return this;
    }

    public boolean isLoginSuccess() {
        return isPresent(LBL_LOGIN_SUCCESS);
    }

    public String getTextErrorMessageCredentials() {
        return textOf(LBL_ENTER_CREDS_CORRECT);
    }

    public String getUserNameValue() {
        return valueOf(TXT_USERNAME);
    }

    public String getPasswordValue() {
        return valueOf(TXT_PASSWORD);
    }

    public HeaderPage focusOnHeader() {
        return new HeaderPage(driver);
    }
}
