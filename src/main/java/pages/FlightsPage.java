package pages;

import utils.enums.Profile;
import utils.interfaces.IFlightDetails;
import utils.interfaces.IFlightPreferences;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Logger;
import utils.Target;

public final class FlightsPage extends BasePage {

    private final Target BTN_CONTINUE =
            Target.name("continue button")
                    .located(By.xpath("//input[@name='findFlights']"));
    private final Target RDO_FLIGHT_TYPE =
            Target.name("type flight radio groupc")
                    .located(By.xpath("//input[@type='radio'][@name='tripType']"));
    private final Target DRP_PASSENGER =
            Target.name("passenger dropdown")
                    .located(By.xpath("//select[@name='passCount']"));
    private final Target DRP_DEPARTING_FROM =
            Target.name("departing from dropdown")
                    .located(By.xpath("//select[@name='fromPort']"));
    private final Target DRP_ON_MONTH =
            Target.name("on month dropdown")
                    .located(By.xpath("//select[@name='fromMonth']"));
    private final Target DRP_ON_DAY =
            Target.name("on month dropdown")
                    .located(By.xpath("//select[@name='fromDay']"));
    private final Target DRP_ARRIVING_IN =
            Target.name("arriving in dropdown")
                    .located(By.xpath("//select[@name='toPort']"));
    private final Target DRP_RETURNING_MONTH =
            Target.name("arriving in dropdown")
                    .located(By.xpath("//select[@name='toMonth']"));
    private final Target DRP_RETURNING_DAY =
            Target.name("arriving in dropdown")
                    .located(By.xpath("//select[@name='toDay']"));
    private final Target RDO_SERVICE_CLASS =
            Target.name("arriving in dropdown")
                    .located(By.xpath("//input[@name='servClass']"));
    private final Target DRP_AIRLINE =
            Target.name("arriving in dropdown")
                    .located(By.xpath("//select[@name='airline']"));

    public FlightsPage(WebDriver driver) {
        super(driver);
    }

    public FlightsPage visit() {
        get(Profile.BASE_URL_WEB.value() + "/test/newtours/reservation.php");

        return this;
    }

    public FlightsPage fillUpFlightDetails(IFlightDetails details) {
        Logger.info("---------Filling up flight details---------");
        selectRadioOptionByIndex(RDO_FLIGHT_TYPE, details.getType());
        selectByValue(DRP_PASSENGER, details.getPassengers());
        selectByValue(DRP_DEPARTING_FROM, details.getDepartingFrom());

        String[] onSplited = details.getOn().split(" ");
        selectByText(DRP_ON_MONTH, onSplited[0]);
        selectByValue(DRP_ON_DAY, onSplited[1]);

        selectByValue(DRP_ARRIVING_IN, details.getArrivingIn());

        String[] returningSplitted = details.getReturning().split(" ");
        selectByText(DRP_RETURNING_MONTH, returningSplitted[0]);
        selectByValue(DRP_RETURNING_DAY, returningSplitted[1]);

        return this;
    }

    public FlightsPage fillUpPreferences(IFlightPreferences preferences) {
        Logger.info("---------Filling up flight preferences---------");
        selectRadioOptionByIndex(RDO_SERVICE_CLASS, preferences.getService());
        selectByText(DRP_AIRLINE, preferences.getAirline());

        return this;
    }

    public NoSeatsPage continueNextPage() {
        clickOn(BTN_CONTINUE);

        return new NoSeatsPage(driver);
    }
}
