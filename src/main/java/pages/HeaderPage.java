package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Target;

public final class HeaderPage extends BasePage {

    private final Target TAB_SIGN_OFF =
            Target.name("sign off tab")
                    .located(By.xpath("//a[text()='SIGN-OFF']"));
    private final Target TAB_SIGN_ON =
            Target.name("sign on tab")
                    .located(By.xpath("//a[text()='SIGN-ON']"));
    private final Target TAB_REGISTER =
            Target.name("register tab")
                    .located(By.xpath("//a[text()='REGISTER']"));

    public HeaderPage(WebDriver driver) {
        super(driver);
    }

    public void selectSignOffTab() {
        clickOn(TAB_SIGN_OFF);
    }

    public void selectSignOnTab() {
        clickOn(TAB_SIGN_ON);
    }

    public RegisterPage selectRegisterTab() {
        clickOn(TAB_REGISTER);

        return new RegisterPage(driver);
    }
}
