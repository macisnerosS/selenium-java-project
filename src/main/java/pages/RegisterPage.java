package pages;

import utils.Logger;
import utils.enums.Profile;
import utils.interfaces.IAccount;
import utils.interfaces.IContact;
import utils.interfaces.IMailing;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Target;

public final class RegisterPage extends BasePage {

    private final Target LBL_DESCRIPTION =
            Target.name("description label")
                    .located(By.xpath("//font[contains(.,'To create your account')]"));
    private final Target BTN_ENVIAR =
            Target.name("enviar button")
                    .located(By.xpath("//input[@name='submit']"));
    private final Target TXT_FIRST_NAME =
            Target.name("first name textbox")
                    .located(By.xpath("//input[@name='firstName']"));
    private final Target TXT_LAST_NAME =
            Target.name("last name textbox")
                    .located(By.xpath("//input[@name='lastName']"));
    private final Target TXT_PHONE =
            Target.name("phone textbox")
                    .located(By.xpath("//input[@name='phone']"));
    private final Target TXT_EMAIL =
            Target.name("email textbox")
                    .located(By.xpath("//input[@id='userName']"));
    private final Target LBL_THANKS_REGISTER =
            Target.name("thank you for registering label")
                    .located(By.xpath("//font[contains(., 'Thank you for registering')]"));
    private final Target LBL_NOTE =
            Target.name("note label")
                    .located(By.xpath("//b[contains(., 'Note:')]"));
    private final Target TXT_ADDRESS =
            Target.name("address textbox")
                    .located(By.xpath("//input[@name='address1']"));
    private final Target TXT_CITY =
            Target.name("city textbox")
                    .located(By.xpath("//input[@name='city']"));
    private final Target TXT_STATE =
            Target.name("state textbox")
                    .located(By.xpath("//input[@name='state']"));
    private final Target TXT_POSTAL_CODE =
            Target.name("postal code textbox")
                    .located(By.xpath("//input[@name='postalCode']"));
    private final Target TXT_USERNAME =
            Target.name("username textbox")
                    .located(By.xpath("//input[@name='email']"));
    private final Target TXT_PASSWORD =
            Target.name("password textbox")
                    .located(By.xpath("//input[@name='password']"));
    private final Target TXT_CONFIRM_PASSWORD =
            Target.name("confirm password textbox")
                    .located(By.xpath("//input[@name='confirmPassword']"));
    private final Target DRP_COUNTRY =
            Target.name("country dropdown")
                    .located(By.xpath("//select[@name='country']"));

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public RegisterPage visit() {
        get(Profile.BASE_URL_WEB.value() + "/test/newtours/register.php");

        return this;
    }

    public String getTextDescription() {
        return textOf(LBL_DESCRIPTION);
    }

    public RegisterPage submitFormulary() {
        clickOn(BTN_ENVIAR);

        return this;
    }

    public RegisterPage fillUpContactInformation(IContact contact) {
        Logger.info("----------Filling up contact information----------");
        set(TXT_FIRST_NAME, contact.getName());
        set(TXT_LAST_NAME, contact.getLastname());
        set(TXT_PHONE, contact.getPhone());
        set(TXT_EMAIL, contact.getEmail());

        return this;
    }

    public RegisterPage fillUpMailingInformation(IMailing mailing) {
        Logger.info("----------Filling up mailing information----------");
        set(TXT_ADDRESS, mailing.getAddress());
        set(TXT_CITY, mailing.getCity());
        set(TXT_STATE, mailing.getState());
        set(TXT_POSTAL_CODE, mailing.getPostalCode());
        selectByValue(DRP_COUNTRY, mailing.getCountry());

        return this;
    }

    public RegisterPage fillUpUserInformation(IAccount account) {
        Logger.info("----------Filling up user information----------");
        set(TXT_USERNAME, account.getUsername());
        set(TXT_PASSWORD, account.getPassword());
        set(TXT_CONFIRM_PASSWORD, account.getConfPassword());

        return this;
    }

    public String getTextOfThanksForRegister() {
        return textOf(LBL_THANKS_REGISTER);
    }

    public String getTextSuccessfulNote() {
        return textOf(LBL_NOTE);
    }
}
