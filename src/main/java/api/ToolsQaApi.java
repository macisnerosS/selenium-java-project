package api;

import utils.enums.Profile;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import api.handler.ToolsQaApiHandler;
import api.payload.AccountPayload;

import java.util.HashMap;

public class ToolsQaApi extends BaseApi {

    // Bookstore controller
    private final String BOOKSTORE_V1_BOOKS = "/BookStore/v1/Books";
    private final String BOOKSTORE_V1_BOOK = "/BookStore/v1/Book";
    private final String BOOKSTORE_V1_BOOKS_$ISBN = "/BookStore/v1/Books/{isbn}";

    // Account controller
    private final String ACCOUNT_V1_AUTHORIZED = "/Account/v1/Authorized";
    private final String ACCOUNT_V1_USER = "/Account/v1/User";
    private final String ACCOUNT_V1_GENERATETOKEN = "/Account/v1/GenerateToken";
    private final String ACCOUNT_V1_USER_$UUID = "/Account/v1/User/{uuid}";

    private static ToolsQaApi instance = null;
    private ToolsQaApiHandler handler = null;

    private ToolsQaApi() {
    }

    public static ToolsQaApi rest() {
        RestAssured.baseURI = Profile.BASE_URL_API.value();

        if (instance == null) {
            instance = new ToolsQaApi();
        }

        return instance;
    }

    public ToolsQaApiHandler handler() {
        if (handler == null) {
            handler = new ToolsQaApiHandler(instance);
        }

        return handler;
    }

    public Response getBookstoreV1Books(final int statusCode) {
        return get(BOOKSTORE_V1_BOOKS, statusCode);
    }

    public Response getBookstoreV1Book(HashMap queryParams, final int statusCode) {
        return get(BOOKSTORE_V1_BOOK, queryParams, statusCode);
    }

    public Response postAccountV1Authorized(Object body, final int statusCode) {
        return post(body, ACCOUNT_V1_AUTHORIZED, statusCode);
    }

    public Response postAccountV1GenerateToken(Object body, final int statusCode) {
        return post(body, ACCOUNT_V1_GENERATETOKEN, statusCode);
    }

    public Response postAccountV1User(AccountPayload body, final int statusCode) {
        return post(body, ACCOUNT_V1_USER, statusCode);
    }
}
