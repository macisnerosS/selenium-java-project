package api.handler;

import api.payload.AccountPayload;
import api.response.AllBooksResponse;
import api.ToolsQaApi;
import api.response.BookResponse;
import api.response.CreateUserResponse;
import api.response.ErrorMessageResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ToolsQaApiHandler {

    private static ToolsQaApi service = null;

    public ToolsQaApiHandler(ToolsQaApi service) {
        this.service = service;
    }

    public List<BookResponse> getListOfBooks() {
        return service.getBookstoreV1Books(200)
                .as(AllBooksResponse.class).getBooks();
    }

    public BookResponse findBookByAuthor(String name) {
        return getListOfBooks().stream()
                .filter(book -> book.getAuthor().equals(name))
                .findFirst().orElse(null);
    }

    public BookResponse pickAnyBook() {
        List<BookResponse> books = getListOfBooks();

        Random rand = new Random();
        int randomNumber = rand.nextInt(books.size());

        return books.get(randomNumber);
    }

    public ErrorMessageResponse catchNotAvailableIsbn(String isbn) {
        HashMap<String, String> queryParams = new HashMap() {{
            put("ISBN", isbn);
        }};

        return service.getBookstoreV1Book(queryParams, 400)
                .as(ErrorMessageResponse.class);
    }

    public ErrorMessageResponse catchAlreadyCreatedAccount(Object payload) {
        return service.postAccountV1User((AccountPayload) payload, 406)
                .as(ErrorMessageResponse.class);
    }

    public CreateUserResponse createNewAccount(Object payload) {
        return service.postAccountV1User((AccountPayload) payload, 201)
                .as(CreateUserResponse.class);
    }
}
