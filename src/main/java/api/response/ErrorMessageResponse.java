package api.response;

import lombok.Data;

@Data
public class ErrorMessageResponse {
    private String code;
    private String message;
}
