package api.response;

import lombok.Data;

import java.util.List;

@Data
public class CreateUserResponse {
    private String userID;
    private String username;
    private List<BookResponse> books;
}
