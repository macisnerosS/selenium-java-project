package api.response;

import lombok.Data;

import java.util.List;

@Data
public class AllBooksResponse {
    private List<BookResponse> books;
}

