package api;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;

import static io.restassured.RestAssured.given;

public abstract class BaseApi {

    protected static Response get(String path, int statusCode) {
        RequestSpecification basedOnConditions = given()
                .basePath(path);

        return checkStatusCodeAndGetResponse(basedOnConditions.when().get(), statusCode);
    }

    protected static Response get(String path, HashMap params, int statusCode) {
        RequestSpecification basedOnConditions = given()
                .basePath(path)
                .queryParams(params);

        return checkStatusCodeAndGetResponse(basedOnConditions.when().get(), statusCode);
    }

    protected static Response post(Object body, String path, int statusCode) {
        RequestSpecification basedOnConditions = given()
                .basePath(path)
                .body(body)
                .contentType(ContentType.JSON);

        return checkStatusCodeAndGetResponse(basedOnConditions.when().post(), statusCode);
    }

    protected static Response put(Object body, String path, int statusCode) {
        RequestSpecification basedOnConditions = given()
                .basePath(path)
                .body(body)
                .contentType(ContentType.JSON);

        return checkStatusCodeAndGetResponse(basedOnConditions.when().put(), statusCode);
    }

    private static Response checkStatusCodeAndGetResponse(Response request, int statusCode) {
        return request.then()
                .assertThat().statusCode(statusCode)
                .and().extract().response();
    }
}
