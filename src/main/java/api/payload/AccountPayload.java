package api.payload;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AccountPayload {
    private String userName;
    private String password;
}
