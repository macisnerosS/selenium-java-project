package data.set;

import utils.interfaces.ICredentials;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserProfileDataSet implements ICredentials {
    private String username;
    private String password;
}
