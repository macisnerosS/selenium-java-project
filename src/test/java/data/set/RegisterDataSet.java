package data.set;

import utils.interfaces.IAccount;
import utils.interfaces.IContact;
import utils.interfaces.IMailing;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RegisterDataSet implements IContact, IMailing, IAccount {
    private String name;
    private String lastname;
    private String phone;
    private String email;
    private String address;
    private String city;
    private String postalCode;
    private String state;
    private String country;
    private String username;
    private String password;
    private String confPassword;
}
