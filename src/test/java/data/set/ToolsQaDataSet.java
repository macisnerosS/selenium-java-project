package data.set;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ToolsQaDataSet {
    private Object payload;
    private String username;
}
