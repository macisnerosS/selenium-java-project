package data.set;

import utils.interfaces.IFlightPreferences;
import lombok.Builder;
import lombok.Getter;
import utils.interfaces.IFlightDetails;

@Builder
@Getter
public class FlightInformationDataSet implements IFlightDetails, IFlightPreferences {
    private String passengers;
    private String departingFrom;
    private String airline;
    private String on;
    private String arrivingIn;
    private String returning;
    private Integer type;
    private Integer service;
}
