package data.set;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class BookstoreDataSet {
    private String isbn;
}
