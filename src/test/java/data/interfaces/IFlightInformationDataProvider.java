package data.interfaces;

import org.testng.annotations.DataProvider;

public interface IFlightInformationDataProvider {
    String VALID_FLIGHT_INFORMATION = "1";
    @DataProvider(name = VALID_FLIGHT_INFORMATION)
    Object[][] validFlightInformation();
}
