package data.interfaces;

import data.set.UserProfileDataSet;
import org.testng.annotations.DataProvider;

public interface IUserProfileDataProvider {
    String CORRECT_CREDENTIAL = "1";
    String WRONG_CREDENTIAL = "2";

    String ANY_CREDENTIAL_IS_BLANK = "3";
    String ONLY_ONE_CREDENTIAL_WRONG = "4";
    String COMBINATION_CHARS = "5";

    @DataProvider(name = ANY_CREDENTIAL_IS_BLANK)
    Object[][] anyCredentialIsBlank();

    @DataProvider(name = CORRECT_CREDENTIAL)
    Object[][] correctCredential();

    @DataProvider(name = WRONG_CREDENTIAL)
    Object[][] wrongCredential();

    @DataProvider(name = ONLY_ONE_CREDENTIAL_WRONG)
    Object[][] onlyOneCredentialWrong();

    @DataProvider(name = COMBINATION_CHARS)
    Object[][] combinationChars();
}
