package data.interfaces;

import org.testng.annotations.DataProvider;

public interface IBookstoreDataProvider {
    String FAKE_ISBN = "0";
    String BLANK_ISBN = "1";
    String SPECIAL_CHAR_ISBN = "2";
    String NUMBER_ISBN = "3";

    @DataProvider(name = FAKE_ISBN)
    public Object[][] fakeIsbn();

    @DataProvider(name = BLANK_ISBN)
    public Object[][] blankIsbn();

    @DataProvider(name = SPECIAL_CHAR_ISBN)
    public Object[][] specialCharIsbn();

    @DataProvider(name = NUMBER_ISBN)
    public Object[][] numberIsbn();
}
