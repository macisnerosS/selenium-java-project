package data.providers.stg;

import data.interfaces.IAccountDataProvider;
import data.set.ToolsQaDataSet;
import org.testng.annotations.DataProvider;
import api.payload.AccountPayload;
import utils.Sample;

public class ToolsQaDataProvider implements IAccountDataProvider {
    private final String DEFAULT_PASSWORD = "tes23t$#%@TMFSA";
    private final String DEFAULT_USERNAME = "testSTG";

    @DataProvider(name = EXISTING_ACCOUNT)
    public Object[][] alreadyCreatedAccount() {
        return new Object[][]{{
                ToolsQaDataSet.builder()
                        .payload(AccountPayload.builder()
                                .userName(DEFAULT_USERNAME)
                                .password(DEFAULT_PASSWORD)
                                .build())
                        .build()
        }};
    }

    @DataProvider(name = AUTO_GENERATED_ACCOUNT)
    @Override
    public Object[][] autoGeneratedAccount() {
        return new Object[][]{{
                ToolsQaDataSet.builder()
                        .username(DEFAULT_USERNAME + Sample.string())
                        .payload(AccountPayload.builder()
                                .userName(DEFAULT_USERNAME + Sample.string())
                                .password(DEFAULT_PASSWORD)
                                .build())
                        .build()
        }};
    }
}
