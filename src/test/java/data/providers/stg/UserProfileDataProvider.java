package data.providers.stg;

import data.interfaces.IUserProfileDataProvider;
import data.set.UserProfileDataSet;
import org.testng.annotations.DataProvider;

public class UserProfileDataProvider implements IUserProfileDataProvider {

    @DataProvider(name = CORRECT_CREDENTIAL)
    public Object[][] correctCredential() {
        return new Object[][]{{
                UserProfileDataSet.builder()
                        .username("guru99")
                        .password("guru99")
                        .build()
        }};
    }

    @DataProvider(name = WRONG_CREDENTIAL, indices = {5})
    public Object[][] wrongCredential() {
        return invalidCredentials();
    }

    @DataProvider(name = ANY_CREDENTIAL_IS_BLANK, indices = {6, 7})
    public Object[][] anyCredentialIsBlank() {
        return invalidCredentials();
    }

    @DataProvider(name = ONLY_ONE_CREDENTIAL_WRONG, indices = {3, 4})
    public Object[][] onlyOneCredentialWrong() {
        return invalidCredentials();
    }

    @DataProvider(name = COMBINATION_CHARS, indices = {0, 1, 2})
    public Object[][] combinationChars() {
        return invalidCredentials();
    }

    public Object[][] invalidCredentials() {
        return new Object[][]{{
                UserProfileDataSet.builder()
                        .username("#%/&=$@")
                        .password("#%/&=$@")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("12345")
                        .password("12345")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("MACS")
                        .password("MACS")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("guru99")
                        .password("guruXX")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("guruXX")
                        .password("guru99")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("guruXX")
                        .password("guruXX")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("guru99")
                        .password("")
                        .build()
        }, {
                UserProfileDataSet.builder()
                        .username("")
                        .password("guru99")
                        .build()
        }};
    }
}
