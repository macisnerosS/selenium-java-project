package data.providers.qa;

import data.interfaces.IFlightInformationDataProvider;
import data.set.FlightInformationDataSet;
import org.testng.annotations.DataProvider;

public class FlightInformationDataProvider implements IFlightInformationDataProvider {

    @DataProvider(name = VALID_FLIGHT_INFORMATION)
    public Object[][] validFlightInformation() {
        return new Object[][]{{
                FlightInformationDataSet.builder()
                        .airline("Unified Airlines")
                        .type(0)
                        .service(2)
                        .passengers("3")
                        .departingFrom("Frankfurt")
                        .on("March 16")
                        .arrivingIn("London")
                        .returning("January 14")
                        .build()
        }};
    }

}
