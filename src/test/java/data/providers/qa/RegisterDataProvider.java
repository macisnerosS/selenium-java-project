package data.providers.qa;

import data.interfaces.IRegisterDataProvider;
import data.set.RegisterDataSet;
import org.testng.annotations.DataProvider;

public class RegisterDataProvider implements IRegisterDataProvider {

    @DataProvider(name = REGISTER_SUCCESS)
    public Object[][] registerSuccess() {
        return new Object[][]{{
                RegisterDataSet.builder()
                        .name("name")
                        .lastname("last")
                        .phone("55555")
                        .email("test@gmail.com")
                        .city("Lima")
                        .state("Lima")
                        .postalCode("Lima 01")
                        .country("PERU")
                        .address("test street")
                        .username("mcisneros")
                        .password("pwd")
                        .confPassword("pwd")
                        .build()
        }};
    }
}
