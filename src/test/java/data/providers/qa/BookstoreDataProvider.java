package data.providers.qa;

import data.interfaces.IBookstoreDataProvider;
import data.set.BookstoreDataSet;
import org.testng.annotations.DataProvider;

public class BookstoreDataProvider implements IBookstoreDataProvider {

    @DataProvider(name = FAKE_ISBN, indices = {0})
    public Object[][] fakeIsbn() {
        return invalidIsbnValues();
    }

    @DataProvider(name = BLANK_ISBN, indices = {2, 5})
    public Object[][] blankIsbn() {
        return invalidIsbnValues();
    }

    @DataProvider(name = SPECIAL_CHAR_ISBN, indices = {3})
    public Object[][] specialCharIsbn() {
        return invalidIsbnValues();
    }

    @DataProvider(name = NUMBER_ISBN, indices = {4})
    public Object[][] numberIsbn() {
        return invalidIsbnValues();
    }

    public Object[][] invalidIsbnValues() {
        return new Object[][]{{
                BookstoreDataSet.builder()
                        .isbn("fake_isbn")
                        .build()
        }, {
                BookstoreDataSet.builder()
                        .isbn("  ")
                        .build()
        }, {
                BookstoreDataSet.builder()
                        .isbn("$!#$@")
                        .build()
        }, {
                BookstoreDataSet.builder()
                        .isbn("1345")
                        .build()
        }, {
                BookstoreDataSet.builder()
                        .isbn("")
                        .build()
        }}; // could add 1 more set for SQL injection
    }
}
