package tests.api.accountcontroller;

import api.ToolsQaApi;
import api.payload.AccountPayload;
import api.response.CreateUserResponse;
import data.providers.qa.ToolsQaDataProvider;
import data.set.ToolsQaDataSet;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import api.response.ErrorMessageResponse;
import utils.Logger;
import utils.enums.Schema;

public class CreateUserTest {

    private final String USER_EXIST_MSG = "User exists!";
    private final String USER_EXIST_CODE = "1204";

    @Test(groups = {"regression"},
            dataProviderClass = ToolsQaDataProvider.class,
            dataProvider = ToolsQaDataProvider.AUTO_GENERATED_ACCOUNT)
    public void testResponseMatchWithJsonSchema(ToolsQaDataSet data) {
        Response response =
                ToolsQaApi.rest()
                        .postAccountV1User((AccountPayload) data.getPayload(), 201);

        Schema.CREATE_USER.matches(response);
    }

    @Test(groups = {"regression"},
            dataProviderClass = ToolsQaDataProvider.class,
            dataProvider = ToolsQaDataProvider.AUTO_GENERATED_ACCOUNT)
    public void testCreateNewAccountSuccess(ToolsQaDataSet data) {
        CreateUserResponse user =
                ToolsQaApi.rest().handler().createNewAccount(data.getPayload());

        Assert.assertEquals(user.getUsername(), user.getUsername(),
                Logger.info("ASSERT: User id matches input data"));
        Assert.assertTrue(user.getBooks().isEmpty(),
                Logger.info("ASSERT: Books of new account is empty"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = ToolsQaDataProvider.class,
            dataProvider = ToolsQaDataProvider.EXISTING_ACCOUNT)
    public void testErrorMessageForAlreadyExistAccount(ToolsQaDataSet data) {
        ErrorMessageResponse error =
                ToolsQaApi.rest().handler().catchAlreadyCreatedAccount(data.getPayload());

        Assert.assertEquals(error.getMessage(), USER_EXIST_MSG,
                Logger.info("ASSERT: Error message is expected"));
        Assert.assertEquals(error.getCode(), USER_EXIST_CODE,
                Logger.info("ASSERT: Error code is expected"));
    }
}
