package tests.api.bookstorecontroller;

import api.ToolsQaApi;
import data.providers.qa.BookstoreDataProvider;
import data.set.BookstoreDataSet;
import utils.enums.Schema;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import api.response.BookResponse;
import api.response.ErrorMessageResponse;
import utils.Logger;

import java.util.HashMap;

public class GetBookByIdTest {

    private final String NOT_AVAILABLE_ISBN_MSG = "ISBN supplied is not available in Books Collection!";
    private final String ERROR_CODE = "1205";

    @Test(groups = {"regression"})
    public void testResponseMatchesWithJsonSchema() {
        BookResponse book = ToolsQaApi.rest().handler().pickAnyBook();

        HashMap<String, String> queryParams = new HashMap() {{
            put("ISBN", book.getIsbn());
        }};

        Response response =
                ToolsQaApi.rest().getBookstoreV1Book(queryParams, 200);

        Schema.BOOK.matches(response);
    }

    @Test(groups = {"regression"},
            dataProviderClass = BookstoreDataProvider.class,
            dataProvider = BookstoreDataProvider.FAKE_ISBN)
    public void testErrorMessageForNonExistingIsbn(BookstoreDataSet data) {
        ErrorMessageResponse response =
                ToolsQaApi.rest().handler().catchNotAvailableIsbn(data.getIsbn());

        Assert.assertEquals(response.getMessage(), NOT_AVAILABLE_ISBN_MSG,
                Logger.info("ASSERT: Error message is expected"));
        Assert.assertEquals(response.getCode(), ERROR_CODE,
                Logger.info("ASSERT: Error code is the expected"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = BookstoreDataProvider.class,
            dataProvider = BookstoreDataProvider.BLANK_ISBN)
    public void testErrorMessageForBlankIsbn(BookstoreDataSet data) {
        ErrorMessageResponse response =
                ToolsQaApi.rest().handler().catchNotAvailableIsbn(data.getIsbn());

        Assert.assertEquals(response.getMessage(), NOT_AVAILABLE_ISBN_MSG,
                Logger.info("ASSERT: error message is expected"));
        Assert.assertEquals(response.getCode(), ERROR_CODE,
                Logger.info("ASSERT: error code is the expected"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = BookstoreDataProvider.class,
            dataProvider = BookstoreDataProvider.NUMBER_ISBN)
    public void testErrorMessageForNumberIsbn(BookstoreDataSet data) {
        ErrorMessageResponse response =
                ToolsQaApi.rest().handler().catchNotAvailableIsbn(data.getIsbn());

        Assert.assertEquals(response.getMessage(), NOT_AVAILABLE_ISBN_MSG,
                Logger.info("ASSERT: error message is expected"));
        Assert.assertEquals(response.getCode(), ERROR_CODE,
                Logger.info("ASSERT: error code is the expected"));
    }
}
