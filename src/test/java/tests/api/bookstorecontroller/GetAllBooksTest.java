package tests.api.bookstorecontroller;

import utils.enums.Schema;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import api.ToolsQaApi;
import api.response.BookResponse;
import utils.Logger;

import java.util.List;

public class GetAllBooksTest {

    @Test(groups = {"regression"})
    public void testResponseMatchesWithJsonSchema() {
        Response response = ToolsQaApi.rest().getBookstoreV1Books(200);

        Schema.BOOKS_LIST.matches(response);
    }

    @Test(groups = {"regression"})
    public void testAllTheBookEndpointIsNotEmpty() {
        List<BookResponse> books = ToolsQaApi.rest().handler().getListOfBooks();

        Assert.assertNotEquals(books.size(), 0,
                Logger.info("ASSERT: Service does not return empty list"));
    }

    @Test(groups = {"regression"})
    public void testCheckTheresNoBooksWithAuthorInBlank() {
        BookResponse book = ToolsQaApi.rest().handler().findBookByAuthor("");

        Assert.assertNull(book,
                Logger.info("ASSERT: There's no any book with author in blank"));
    }
}
