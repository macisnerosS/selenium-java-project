package tests.web;

import data.providers.qa.RegisterDataProvider;
import data.set.RegisterDataSet;
import utils.Driver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.RegisterPage;
import utils.Logger;

public class RegisterTest {
    private final String REGISTER_DESCRIPTION_MSG =
            "To create your account, we'll need some basic information about you. " +
                    "This information will be used to send reservation confirmation emails, mail " +
                    "tickets when needed and contact you if your travel arrangements change. Please " +
                    "fill in the form completely.";
    private final String THANKS_REGISTER_MSG =
            "Thank you for registering. You may now sign-in using " +
                    "the user name and password you've just entered.";
    private final String NOTE_MSG = "Note: Your user name is %s.";

    @Test(groups = {"regression"})
    public void descriptionOnRegisterPageIsDisplayed() {
        RegisterPage registerPage = new RegisterPage(Driver.instance()).visit();

        Assert.assertEquals(registerPage.getTextDescription(), REGISTER_DESCRIPTION_MSG,
                Logger.info("ASSERT: Description text is expected"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = RegisterDataProvider.class,
            dataProvider = RegisterDataProvider.REGISTER_SUCCESS
    )
    public void fillUpFormularyAndSubmitSuccess(RegisterDataSet data) {
        RegisterPage register = new RegisterPage(Driver.instance()).visit();
        RegisterPage filledUpContactInfoPage = register.fillUpContactInformation(data);
        RegisterPage filledUpMailingInfoPage = filledUpContactInfoPage.fillUpMailingInformation(data);
        RegisterPage filledUpFormularyPage = filledUpMailingInfoPage.fillUpUserInformation(data);

        RegisterPage successRegisterPage = filledUpFormularyPage.submitFormulary();

        Assert.assertEquals(successRegisterPage.getTextOfThanksForRegister(), THANKS_REGISTER_MSG,
                Logger.info("ASSERT: Text of thank you for registering is correct"));
        Assert.assertEquals(register.getTextSuccessfulNote(),
                String.format(NOTE_MSG, data.getUsername()),
                Logger.info("ASSERT: Expected message is displayed"));
    }
}
