package tests.web;

import data.providers.qa.FlightInformationDataProvider;
import data.set.FlightInformationDataSet;
import org.testng.Assert;
import pages.NoSeatsPage;
import utils.Driver;
import org.testng.annotations.Test;
import pages.FlightsPage;
import utils.Logger;

public class FindFlightsTest {

    @Test(groups = {"regression"},
            dataProviderClass = FlightInformationDataProvider.class,
            dataProvider = FlightInformationDataProvider.VALID_FLIGHT_INFORMATION)
    public void testUserIsNotAbleToFindSeat(FlightInformationDataSet data) {
        FlightsPage flightsPage = new FlightsPage(Driver.instance()).visit();
        FlightsPage filledFlightDetailsPage = flightsPage.fillUpFlightDetails(data);
        FlightsPage filledFormularyPage = filledFlightDetailsPage.fillUpPreferences(data);

        NoSeatsPage noSeatsPage = filledFormularyPage.continueNextPage();

        Assert.assertTrue(noSeatsPage.isDisplayed(),
                Logger.info("ASSERT: No seats available page is displayed properly"));
    }
}
