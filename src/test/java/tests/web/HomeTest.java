package tests.web;

import data.providers.qa.UserProfileDataProvider;
import data.set.UserProfileDataSet;
import utils.enums.Profile;
import pages.RegisterPage;
import utils.Driver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HeaderPage;
import pages.HomePage;
import utils.Logger;

public class HomeTest {

    private final String HOME_URL = Profile.BASE_URL_WEB.value() + "/test/newtours/index.php";
    private final String REGISTER_URL = Profile.BASE_URL_WEB.value() + "/test/newtours/register.php";
    private final String CREDS_WRONG_MSG = "Enter your userName and password correct";

    @Test(groups = {"regression"})
    public void testGetHomePage() {
        HomePage homePage = new HomePage(Driver.instance()).visit();

        Assert.assertEquals(homePage.isLocatedAt(), HOME_URL,
                Logger.info("Assert url of home page is correct"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = UserProfileDataProvider.class,
            dataProvider = UserProfileDataProvider.CORRECT_CREDENTIAL)
    public void testLoginSuccessfully(UserProfileDataSet data) {
        HomePage homePage = new HomePage(Driver.instance()).visit();
        HomePage loginSuccessPage = homePage.login(data);

        Assert.assertTrue(loginSuccessPage.isLoginSuccess(),
                Logger.info("ASSERT: Login was a success"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = UserProfileDataProvider.class,
            dataProvider = UserProfileDataProvider.ONLY_ONE_CREDENTIAL_WRONG)
    public void testErrorMessageWhenAnyCredIsWrong(UserProfileDataSet data) {
        HomePage homePage = new HomePage(Driver.instance()).visit();
        HomePage loginFailedPage = homePage.loginWithWrongCreds(data);

        Assert.assertEquals(loginFailedPage.getTextErrorMessageCredentials(), CREDS_WRONG_MSG,
                Logger.info("ASSERT: Error message is the expected"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = UserProfileDataProvider.class,
            dataProvider = UserProfileDataProvider.ANY_CREDENTIAL_IS_BLANK)
    public void testErrorMessageWhenAnyCredentialIsBlank(UserProfileDataSet data) {
        HomePage homePage = new HomePage(Driver.instance()).visit();
        HomePage loginFailedPage = homePage.loginWithWrongCreds(data);

        Assert.assertEquals(loginFailedPage.getTextErrorMessageCredentials(), CREDS_WRONG_MSG,
                Logger.info("ASSERT: Error message is the expected"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = UserProfileDataProvider.class,
            dataProvider = UserProfileDataProvider.COMBINATION_CHARS)
    public void testUserNameSupportsAndKeepCombinationOfCharsAfterLogin(UserProfileDataSet data) {
        HomePage homePage = new HomePage(Driver.instance()).visit();
        HomePage filledUpCredsPage = homePage.fillUpCredentials(data);

        Assert.assertEquals(filledUpCredsPage.getUserNameValue(), data.getUsername(),
                Logger.info("ASSERT: Username is the one written"));
    }

    @Test(groups = {"regression"},
            dataProviderClass = UserProfileDataProvider.class,
            dataProvider = UserProfileDataProvider.COMBINATION_CHARS)
    public void testPasswordSupportsAndKeepCombinationOfCharsAfterLogin(UserProfileDataSet data) {
        HomePage homePage = new HomePage(Driver.instance()).visit();
        HomePage filledUpCredsPage = homePage.fillUpCredentials(data);

        Assert.assertEquals(filledUpCredsPage.getPasswordValue(), data.getPassword());
    }

    @Test(groups = {"regression"})
    public void testGetRegisterPageSuccess() {
        HomePage homePage = new HomePage(Driver.instance()).visit();
        HeaderPage headerPage = homePage.focusOnHeader();
        RegisterPage registerPage = headerPage.selectRegisterTab();

        Assert.assertEquals(registerPage.isLocatedAt(), REGISTER_URL,
                Logger.info("ASSERT: Url of register page is correct"));
    }
}
